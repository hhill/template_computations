#ifndef TEMPLATE_COMPUTATIONS_SQUARE_ROOT_H
#define TEMPLATE_COMPUTATIONS_SQUARE_ROOT_H_

template <int N, int Low = 1, int Upp = N>
struct square_root{
    static const int mean = (Low + Upp )/ 2;
    static const bool down = ((mean * mean) >= N);

    static const int value = square_root<N, (down ? Low : mean + 1),
                                               (down ? mean: Upp)>::value;

};

template <int N, int Mid>
struct square_root<N, Mid, Mid>
{
    static const int value = Mid;
};


#ifdef TEST
void test(){
    static_assert(square_root<1>::value == 1, "Error in square_root");
    static_assert(square_root<1>::value == 1, "Error in square_root");
    static_assert(square_root<10, 1, 10>::value == 4, "Error in square_root");

}
#endif

#endif
