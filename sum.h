template <unsigned...> struct sum;

template <unsigned size>
struct sum<size>{
    enum { value = size };
};

template <unsigned size, unsigned... sizes>
struct sum<size, sizes...>{
    enum { value = size + sum<sizes...>::value };
};

static_assert(sum<1,2,3>::value == 6, "");
