#ifndef TEMPLATE_COMPUTATIONS_FACTORIAL_H_
#define TEMPLATE_COMPUTATIONS_FACTORIAL_H_

template <int N>
struct factorial{
    enum {  value = N * factorial<N-1>::value };
};

template <>
struct factorial<0>{
    enum { value = 1 };
};

#ifdef TEST 
void test(){
    static_assert(factorial<0>::value == 1, "Error in factorial");
    static_assert(factorial<1>::value == 1, "Error in factorial");
    static_assert(factorial<10>::value == 3628800, "Error in factorial");
}
#endif

#endif

