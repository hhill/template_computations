#include <type_traits>

template <bool A, bool B>
struct
And{
    enum {value = (A and B)};
};

template <bool A, bool B>
struct
Or{
    enum {value = (A or B)};
};

template <bool A>
struct
Not{
    enum {value = not A};
};

template <typename A>
struct Maybe{
    typedef A value;
    typedef void nothing;
};

template <typename A, typename B>
struct Either{
    typedef A left;
    typedef B right;
};

template <typename X, typename... Xs>
struct
List{

};

#ifdef TEST
void test(){
// And
    static_assert(And<true, true>::value == true, "Error in And.");
    static_assert(And<true, false>::value == false, "Error in And.");
    static_assert(And<false, true>::value == false, "Error in And.");
    static_assert(And<false, false>::value == false, "Error in And.");
// Or
    static_assert(Or<true, true>::value == true, "Error in Or.");
    static_assert(Or<false, true>::value == true, "Error in Or.");
    static_assert(Or<true, false>::value == true, "Error in Or.");
    static_assert(Or<false, false>::value == false, "Error in Or.");
// Not
    static_assert(Not<true>::value == false, "Error in Not.");
    static_assert(Not<false>::value == true, "Error in Not.");
    static_assert(Not<Not<true>::value>::value == true, "Error in Not.");

// Maybe 
    static_assert(std::is_same<Maybe<int>::value, int>::value, "Error in Maybe.");
    static_assert(std::is_same<Maybe<int>::nothing, void>::value, "Error in Maybe.");

// Either
    static_assert(std::is_same<Either<int, double>::left, int>::value, "Error in Either.");
    static_assert(std::is_same<Either<int, double>::right, double>::value, "Error in Either.");

}
#endif

