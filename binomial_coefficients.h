#ifndef TEMPLATE_COMPUTATIONS_BINOMIAL_COEFFICIENTS_H_
#define TEMPLATE_COMPUTATIONS_BINOMIAL_COEFFICIENTS_H_

template <int N, int K>
struct binomial_coefficient
{
    enum { value = binomial_coefficient<N - 1, K - 1>::value 
                    + binomial_coefficient<N - 1, K>::value };
};

template <int N>
struct binomial_coefficient<N, N>
{
        enum { value = 1 };
};

template <int N>
struct binomial_coefficient<N, 0>
{
      enum { value = N > 0 ? 1 : 0 };
};

template <int K>
struct binomial_coefficient<0, K>
{
      enum { value = K > 0 ? 1 : 0 };
};

template<>
struct binomial_coefficient<0, 0>
{
      enum { value = 1 };
};

#ifdef TEST
void test(){
    static_assert(binomial_coefficient<2, 0>::value == 1, "");
    static_assert(binomial_coefficient<2, 1>::value == 2, "");
    static_assert(binomial_coefficient<2, 2>::value == 1, "");

    static_assert(binomial_coefficient<5, 0>::value == 1, "");
    static_assert(binomial_coefficient<5, 1>::value == 5, "");
    static_assert(binomial_coefficient<5, 2>::value == 10, "");
    static_assert(binomial_coefficient<5, 3>::value == 10, "");
    static_assert(binomial_coefficient<5, 4>::value == 5, "");
    static_assert(binomial_coefficient<5, 5>::value == 1, "");

}
#endif

#endif
